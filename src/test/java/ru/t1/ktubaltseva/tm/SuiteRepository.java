package ru.t1.ktubaltseva.tm;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.t1.ktubaltseva.tm.unit.repository.dto.ProjectDTORepositoryTest;
import ru.t1.ktubaltseva.tm.unit.repository.dto.TaskDTORepositoryTest;
import ru.t1.ktubaltseva.tm.unit.repository.dto.UserDTORepositoryTest;
import ru.t1.ktubaltseva.tm.unit.repository.model.ProjectRepositoryTest;
import ru.t1.ktubaltseva.tm.unit.repository.model.TaskRepositoryTest;
import ru.t1.ktubaltseva.tm.unit.repository.model.UserRepositoryTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ProjectDTORepositoryTest.class, TaskDTORepositoryTest.class, UserDTORepositoryTest.class, ProjectRepositoryTest.class, TaskRepositoryTest.class, UserRepositoryTest.class})
public class SuiteRepository {

}