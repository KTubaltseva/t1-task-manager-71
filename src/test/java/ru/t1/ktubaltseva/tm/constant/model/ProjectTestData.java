package ru.t1.ktubaltseva.tm.constant.model;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class ProjectTestData {

    @Nullable
    public final static Project NULL_MODEL = null;

    @Nullable
    public final static String NULL_MODEL_ID = null;

    @Nullable
    public final static String NON_EXISTENT_MODEL_ID = "NON_EXISTENT_MODEL_ID";

    @Nullable
    public final static Project NON_EXISTENT_MODEL = new Project();

    @NotNull
    public final static Project MODEL_1 = new Project("name1");

    @NotNull
    public final static Project MODEL_2 = new Project("name2");

    @NotNull
    public final static List<Project> MODEL_LIST = Arrays.asList(MODEL_1, MODEL_2);

    @Nullable
    public final static String MODEL_NAME = "MODEL_NAME";

}
