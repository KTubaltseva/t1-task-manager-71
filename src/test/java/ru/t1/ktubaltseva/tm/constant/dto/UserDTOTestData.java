package ru.t1.ktubaltseva.tm.constant.dto;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;

@UtilityClass
public final class UserDTOTestData {

    @Nullable
    public final static String USER_1_LOGIN = "USER_1_LOGIN";

    @Nullable
    public final static String USER_1_PASSWORD = "USER_1_PASSWORD";

    @Nullable
    public final static String USER_1_EMAIL = "USER_1_EMAIL";

    @Nullable
    public final static UserDTO NULL_USER = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static UserDTO NON_EXISTENT_USER = new UserDTO();

    @Nullable
    public final static String USER_EMAIL = "TestEmail";

    @Nullable
    public final static String NON_EXISTENT_USER_EMAIL = "NON_EXISTENT_USER_EMAIL";

    @Nullable
    public final static String NULL_EMAIL = null;

    @Nullable
    public final static String USER_PASSWORD = "TestPassword";

    @Nullable
    public final static String USER_LOGIN = "TestLogin";

    @Nullable
    public final static String NON_EXISTENT_USER_LOGIN = "NON_EXISTENT_USER_LOGIN";

    @Nullable
    public final static String NULL_LOGIN = null;

    @Nullable
    public final static String TEST_USER_LOGIN = "user";

    @Nullable
    public final static String TEST_USER_PASSWORD = "user";

    @Nullable
    public final static String ADMIN_USER_LOGIN = "admin";

    @Nullable
    public final static String ADMIN_USER_PASSWORD = "admin";

}
