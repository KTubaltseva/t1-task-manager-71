package ru.t1.ktubaltseva.tm.constant.model;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;

@UtilityClass
public final class UserTestData {

    @Nullable
    public final static String USER_1_LOGIN = "USER_1_LOGIN";

    @Nullable
    public final static String USER_1_PASSWORD = "USER_1_PASSWORD";

    @Nullable
    public final static String USER_1_EMAIL = "USER_1_EMAIL";

    @Nullable
    public final static Project NULL_PROJECT = null;

    @Nullable
    public final static User NULL_USER = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static User NON_EXISTENT_USER = new User();

    @Nullable
    public final static String USER_EMAIL = "TestEmail";

    @Nullable
    public final static String NON_EXISTENT_USER_EMAIL = "NON_EXISTENT_USER_EMAIL";

    @Nullable
    public final static String NULL_EMAIL = null;

    @Nullable
    public final static String USER_PASSWORD = "TestPassword";

    @Nullable
    public final static String USER_LOGIN = "TestLogin";

    @Nullable
    public final static String NON_EXISTENT_USER_LOGIN = "NON_EXISTENT_USER_LOGIN";

    @Nullable
    public final static String NULL_LOGIN = null;

    @Nullable
    public final static String TEST_USER_LOGIN = "user";

    @Nullable
    public final static String TEST_USER_PASSWORD = "user";

}
