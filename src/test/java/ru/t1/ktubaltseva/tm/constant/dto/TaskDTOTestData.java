package ru.t1.ktubaltseva.tm.constant.dto;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class TaskDTOTestData {

    @Nullable
    public final static TaskDTO NULL_MODEL = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static String NULL_MODEL_ID = null;

    @Nullable
    public final static String NON_EXISTENT_MODEL_ID = "NON_EXISTENT_MODEL_ID";

    @Nullable
    public final static String NULL_PROJECT_ID = null;

    @Nullable
    public final static TaskDTO NON_EXISTENT_MODEL = new TaskDTO();

    @NotNull
    public final static TaskDTO MODEL_1 = new TaskDTO("name1");

    @NotNull
    public final static TaskDTO MODEL_2 = new TaskDTO("name2");

    @NotNull
    public final static List<TaskDTO> MODEL_LIST = Arrays.asList(MODEL_1, MODEL_2);

    @Nullable
    public final static String MODEL_NAME = "MODEL_NAME";

    @NotNull
    public final static ProjectDTO PROJECT_1 = new ProjectDTO("name1");

}
