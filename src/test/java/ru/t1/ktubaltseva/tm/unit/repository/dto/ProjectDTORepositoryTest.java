package ru.t1.ktubaltseva.tm.unit.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.ProjectDTORepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

import static ru.t1.ktubaltseva.tm.constant.dto.ProjectDTOTestData.*;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_LOGIN;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class ProjectDTORepositoryTest {

    @NotNull
    private final ProjectDTO projectWithUser = MODEL_1;

    @NotNull
    private final ProjectDTO projectWithoutUser = MODEL_2;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        projectWithUser.setUserId(UserUtil.getUserId());

        repository.saveAndFlush(projectWithUser);
        repository.saveAndFlush(projectWithoutUser);
    }

    @After
    @SneakyThrows
    public void after() {
        repository.deleteAll();
    }

    @Test
    @SneakyThrows
    public void countByUserId() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteAllByUserId() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByUserIdAndId() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteByUserIdAndId(UserUtil.getUserId(), projectWithUser.getId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    public void existsByUserIdAndId() {
        Assert.assertTrue(repository.existsByUserIdAndId(UserUtil.getUserId(), projectWithUser.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), NON_EXISTENT_MODEL_ID));
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), projectWithoutUser.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final List<ProjectDTO> projects = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndId() {
        Assert.assertTrue(repository.findByUserIdAndId(UserUtil.getUserId(), projectWithUser.getId()).isPresent());
        Assert.assertFalse(repository.findByUserIdAndId(UserUtil.getUserId(), projectWithoutUser.getId()).isPresent());
    }

}
