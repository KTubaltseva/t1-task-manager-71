package ru.t1.ktubaltseva.tm.unit.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.model.UserRepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

import static ru.t1.ktubaltseva.tm.constant.model.TaskTestData.*;
import static ru.t1.ktubaltseva.tm.constant.model.UserTestData.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class TaskServiceTest {

    @NotNull
    private final Task taskWithUser = MODEL_1;

    @NotNull
    private final Task taskWithoutUser = MODEL_2;

    @NotNull
    private final Project project = PROJECT_1;

    @Nullable
    private User testUser;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ITaskService service;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        testUser = userRepository.findById(UserUtil.getUserId()).get();

        taskWithUser.setProject(project);

        projectRepository.saveAndFlush(project);

        service.add(testUser, taskWithUser);
        service.add(taskWithoutUser);
    }

    @After
    @SneakyThrows
    public void after() {
        service.clear();
        projectRepository.delete(project);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void create() {
        Assert.assertEquals(2, service.count());
        service.create();
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_MODEL));

        Assert.assertEquals(2, service.count());
        @NotNull final Task task = new Task(MODEL_NAME);
        service.add(task);
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void create_list() {
        service.clear();
        Assert.assertEquals(0, service.count());
        service.create(MODEL_LIST);
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clear() {
        Assert.assertEquals(2, service.count());
        service.clear();
        Assert.assertEquals(0, service.count());
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_MODEL_ID));

        Assert.assertTrue(service.existsById(taskWithUser.getId()));
        Assert.assertTrue(service.existsById(taskWithoutUser.getId()));
        Assert.assertFalse(service.existsById(NON_EXISTENT_MODEL_ID));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<Task> tasks = service.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(NON_EXISTENT_MODEL_ID));

        Assert.assertNotNull(service.findById(taskWithUser.getId()));
        Assert.assertNotNull(service.findById(taskWithoutUser.getId()));

        Assert.assertEquals(taskWithUser.getId(), service.findById(taskWithUser.getId()).getId());
        Assert.assertEquals(taskWithoutUser.getId(), service.findById(taskWithoutUser.getId()).getId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void delete() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(NON_EXISTENT_MODEL));

        Assert.assertEquals(2, service.count());
        service.delete(taskWithUser);
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.deleteById(NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.deleteById(NON_EXISTENT_MODEL_ID));

        Assert.assertEquals(2, service.count());
        service.deleteById(taskWithUser.getId());
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void set() {
        @NotNull final Task task = new Task(MODEL_NAME);
        service.add(task);
        Assert.assertEquals(3, service.count());
        service.set(MODEL_LIST);
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void update() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NON_EXISTENT_MODEL));

        taskWithUser.setName(MODEL_NAME);
        service.update(taskWithUser);
        Assert.assertEquals(MODEL_NAME, service.findById(taskWithUser.getId()).getName());
    }


    @Test
    @SneakyThrows
    @Transactional
    public void create_WithUser() {
        Assert.assertEquals(2, service.count());
        service.create(testUser);
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add_WithUser() {
        @NotNull final Task task = new Task(MODEL_NAME);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(testUser, NULL_MODEL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.add(NULL_USER, task));

        Assert.assertEquals(2, service.count());
        service.add(testUser, task);
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clear_WithUser() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.clear(NULL_USER));

        service.clear(testUser);
        Assert.assertEquals(0, service.count(testUser));
    }

    @Test
    @SneakyThrows
    public void existsById_WithUser() {
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(testUser, NULL_MODEL_ID));
        Assert.assertThrows(UserNotFoundException.class, () -> service.existsById(NULL_USER, taskWithUser.getId()));

        Assert.assertTrue(service.existsById(testUser, taskWithUser.getId()));
        Assert.assertFalse(service.existsById(testUser, taskWithoutUser.getId()));
        Assert.assertFalse(service.existsById(testUser, NON_EXISTENT_MODEL_ID));
    }

    @Test
    @SneakyThrows
    public void findAll_WithUser() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.findAll(NULL_USER));

        @NotNull final List<Task> tasks = service.findAll(testUser);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findById_WithUser() {
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(testUser, NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(testUser, NON_EXISTENT_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(NON_EXISTENT_USER, taskWithUser.getId()));
        Assert.assertThrows(UserNotFoundException.class, () -> service.findById(NULL_USER, taskWithUser.getId()));

        Assert.assertNotNull(service.findById(testUser, taskWithUser.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(testUser, taskWithoutUser.getId()));

        Assert.assertEquals(taskWithUser.getId(), service.findById(taskWithUser.getId()).getId());
    }

    @Test
    @SneakyThrows
    public void count_WithUser() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.count(NULL_USER));

        Assert.assertEquals(1, service.count(testUser));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void delete_WithUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(testUser, NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(testUser, NON_EXISTENT_MODEL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.delete(NULL_USER, taskWithUser));

        Assert.assertEquals(2, service.count());
        service.delete(testUser, taskWithUser);
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteById_WithUser() {
        Assert.assertThrows(IdEmptyException.class, () -> service.deleteById(testUser, NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.deleteById(testUser, NON_EXISTENT_MODEL_ID));
        Assert.assertThrows(UserNotFoundException.class, () -> service.deleteById(NULL_USER, taskWithUser.getId()));

        Assert.assertEquals(2, service.count());
        service.deleteById(testUser, taskWithUser.getId());
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void update_WithUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(testUser, NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(testUser, NON_EXISTENT_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NON_EXISTENT_USER, taskWithUser));
        Assert.assertThrows(UserNotFoundException.class, () -> service.update(NULL_USER, taskWithUser));

        taskWithUser.setName(MODEL_NAME);
        service.update(testUser, taskWithUser);
        Assert.assertEquals(MODEL_NAME, service.findById(taskWithUser.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void findAllByUserAndProject() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.findAllByUserAndProject(NULL_USER, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findAllByUserAndProject(testUser, NULL_PROJECT));

        @NotNull final List<Task> tasks = service.findAllByUserAndProject(testUser, project);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

}
