package ru.t1.ktubaltseva.tm.unit.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.UserDTORepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

import static ru.t1.ktubaltseva.tm.constant.dto.ProjectDTOTestData.*;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class ProjectDTOServiceTest {

    @NotNull
    private final ProjectDTO projectWithUser = MODEL_1;

    @NotNull
    private final ProjectDTO projectWithoutUser = MODEL_2;

    @Nullable
    private UserDTO testUser;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Autowired
    private UserDTORepository userRepository;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        testUser = userRepository.findById(UserUtil.getUserId()).get();

        service.add(testUser.getId(), projectWithUser);
        service.add(projectWithoutUser);
    }

    @After
    @SneakyThrows
    public void after() {
        service.clear();
    }

    @Test
    @SneakyThrows
    @Transactional
    public void create() {
        Assert.assertEquals(2, service.count());
        service.create();
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_MODEL));

        Assert.assertEquals(2, service.count());
        @NotNull final ProjectDTO project = new ProjectDTO(MODEL_NAME);
        service.add(project);
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void create_list() {
        service.clear();
        Assert.assertEquals(0, service.count());
        service.create(MODEL_LIST);
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clear() {
        Assert.assertEquals(2, service.count());
        service.clear();
        Assert.assertEquals(0, service.count());
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_MODEL_ID));

        Assert.assertTrue(service.existsById(projectWithUser.getId()));
        Assert.assertTrue(service.existsById(projectWithoutUser.getId()));
        Assert.assertFalse(service.existsById(NON_EXISTENT_MODEL_ID));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<ProjectDTO> projects = service.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(NON_EXISTENT_MODEL_ID));

        Assert.assertNotNull(service.findById(projectWithUser.getId()));
        Assert.assertNotNull(service.findById(projectWithoutUser.getId()));

        Assert.assertEquals(projectWithUser.getId(), service.findById(projectWithUser.getId()).getId());
        Assert.assertEquals(projectWithoutUser.getId(), service.findById(projectWithoutUser.getId()).getId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void delete() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(NON_EXISTENT_MODEL));

        Assert.assertEquals(2, service.count());
        service.delete(projectWithUser);
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.deleteById(NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.deleteById(NON_EXISTENT_MODEL_ID));

        Assert.assertEquals(2, service.count());
        service.deleteById(projectWithUser.getId());
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void set() {
        @NotNull final ProjectDTO project = new ProjectDTO(MODEL_NAME);
        service.add(project);
        Assert.assertEquals(3, service.count());
        service.set(MODEL_LIST);
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void update() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NON_EXISTENT_MODEL));

        projectWithUser.setName(MODEL_NAME);
        service.update(projectWithUser);
        Assert.assertEquals(MODEL_NAME, service.findById(projectWithUser.getId()).getName());
    }


    @Test
    @SneakyThrows
    @Transactional
    public void create_WithUserId() {
        Assert.assertEquals(2, service.count());
        service.create(testUser.getId());
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add_WithUserId() {
        @NotNull final ProjectDTO project = new ProjectDTO(MODEL_NAME);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(testUser.getId(), NULL_MODEL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.add(NULL_USER_ID, project));

        Assert.assertEquals(2, service.count());
        service.add(testUser.getId(), project);
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void clear_WithUserId() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.clear(NULL_USER_ID));

        service.clear(testUser.getId());
        Assert.assertEquals(0, service.count(testUser.getId()));
    }

    @Test
    @SneakyThrows
    public void existsById_WithUserId() {
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(testUser.getId(), NULL_MODEL_ID));
        Assert.assertThrows(UserNotFoundException.class, () -> service.existsById(NULL_USER_ID, projectWithUser.getId()));

        Assert.assertTrue(service.existsById(testUser.getId(), projectWithUser.getId()));
        Assert.assertFalse(service.existsById(testUser.getId(), projectWithoutUser.getId()));
        Assert.assertFalse(service.existsById(testUser.getId(), NON_EXISTENT_MODEL_ID));
    }

    @Test
    @SneakyThrows
    public void findAll_WithUserId() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.findAll(NULL_USER_ID));

        @NotNull final List<ProjectDTO> projects = service.findAll(testUser.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @SneakyThrows
    public void findById_WithUserId() {
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(testUser.getId(), NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(testUser.getId(), NON_EXISTENT_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(NON_EXISTENT_USER_ID, projectWithUser.getId()));
        Assert.assertThrows(UserNotFoundException.class, () -> service.findById(NULL_USER_ID, projectWithUser.getId()));

        Assert.assertNotNull(service.findById(testUser.getId(), projectWithUser.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(testUser.getId(), projectWithoutUser.getId()));

        Assert.assertEquals(projectWithUser.getId(), service.findById(projectWithUser.getId()).getId());
    }

    @Test
    @SneakyThrows
    public void count_WithUserId() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.count(NULL_USER_ID));

        Assert.assertEquals(1, service.count(testUser.getId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void delete_WithUserId() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(testUser.getId(), NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(testUser.getId(), NON_EXISTENT_MODEL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.delete(NULL_USER_ID, projectWithUser));

        Assert.assertEquals(2, service.count());
        service.delete(testUser.getId(), projectWithUser);
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteById_WithUserId() {
        Assert.assertThrows(IdEmptyException.class, () -> service.deleteById(testUser.getId(), NULL_MODEL_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.deleteById(testUser.getId(), NON_EXISTENT_MODEL_ID));
        Assert.assertThrows(UserNotFoundException.class, () -> service.deleteById(NULL_USER_ID, projectWithUser.getId()));

        Assert.assertEquals(2, service.count());
        service.deleteById(testUser.getId(), projectWithUser.getId());
        Assert.assertEquals(1, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void update_WithUserId() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(testUser.getId(), NULL_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(testUser.getId(), NON_EXISTENT_MODEL));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NON_EXISTENT_USER_ID, projectWithUser));
        Assert.assertThrows(UserNotFoundException.class, () -> service.update(NULL_USER_ID, projectWithUser));

        projectWithUser.setName(MODEL_NAME);
        service.update(testUser.getId(), projectWithUser);
        Assert.assertEquals(MODEL_NAME, service.findById(projectWithUser.getId()).getName());
    }

}
