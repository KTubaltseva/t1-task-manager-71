package ru.t1.ktubaltseva.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.ktubaltseva.tm.constant.dto.ProjectDTOTestData.MODEL_1;
import static ru.t1.ktubaltseva.tm.constant.dto.ProjectDTOTestData.MODEL_NAME;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_LOGIN;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class ProjectRestEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080/api/projects";

    @NotNull
    private final ProjectDTO project = MODEL_1;

    @Nullable
    private UserDTO testUser;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Before
    @SneakyThrows
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        testUser = userService.findById(UserUtil.getUserId());

        service.add(testUser.getId(), project);
    }

    @After
    @SneakyThrows
    public void after() {
        service.clear();
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = URL + "/create";
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.put(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ProjectDTO projectRs = objectMapper.readValue(jsonRs, ProjectDTO.class);
        Assert.assertNotNull(service.findById(testUser.getId(), projectRs.getId()));
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final String url = URL + "/add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(MODEL_NAME);
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.put(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ProjectDTO projectRs = objectMapper.readValue(jsonRs, ProjectDTO.class);
        Assert.assertNotNull(service.findById(testUser.getId(), project.getId()));
        Assert.assertEquals(projectRs.getId(), project.getId());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = URL + "/delete/" + project.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(testUser.getId(), project.getId()));
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final String url = URL + "/exists/" + project.getId();
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        final boolean rs = objectMapper.readValue(jsonRs, Boolean.class);
        Assert.assertTrue(rs);
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final String url = URL + "/find/" + project.getId();
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ProjectDTO projectRs = objectMapper.readValue(jsonRs, ProjectDTO.class);
        Assert.assertNotNull(service.findById(testUser.getId(), projectRs.getId()));
        Assert.assertEquals(projectRs.getId(), project.getId());
    }

    @Test
    @SneakyThrows
    public void update() {
        @NotNull final String url = URL + "/update";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ProjectDTO project = this.project;
        project.setName(MODEL_NAME);
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.put(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ProjectDTO projectRs = objectMapper.readValue(jsonRs, ProjectDTO.class);
        Assert.assertNotNull(service.findById(testUser.getId(), project.getId()));
        Assert.assertEquals(projectRs.getId(), project.getId());
        Assert.assertEquals(projectRs.getName(), MODEL_NAME);
        Assert.assertEquals(service.findById(testUser.getId(), project.getId()).getName(), MODEL_NAME);
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = URL + "/findAll";
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final List<ProjectDTO> projectsRs = Arrays.asList(objectMapper.readValue(jsonRs, ProjectDTO[].class));
        Assert.assertNotNull(projectsRs);
        Assert.assertEquals(1, projectsRs.size());
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final String url = URL + "/clear";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, service.count());
    }

    @Test
    @SneakyThrows
    public void count() {
        @NotNull final String url = URL + "/count";
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        final long rs = objectMapper.readValue(jsonRs, Long.class);
        Assert.assertEquals(1, rs);
    }

}
