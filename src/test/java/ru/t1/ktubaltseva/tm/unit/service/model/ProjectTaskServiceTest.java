package ru.t1.ktubaltseva.tm.unit.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectTaskService;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.UserRepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import static ru.t1.ktubaltseva.tm.constant.model.TaskTestData.*;
import static ru.t1.ktubaltseva.tm.constant.model.UserTestData.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class ProjectTaskServiceTest {

    @NotNull
    private final Task taskWithUser = MODEL_1;

    @NotNull
    private final Task taskWithoutUser = MODEL_2;

    @NotNull
    private final Project project = PROJECT_1;

    @Nullable
    private User testUser;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectTaskService service;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        testUser = userRepository.findById(UserUtil.getUserId()).get();

        taskWithUser.setProject(project);

        projectService.add(testUser, project);

        taskService.add(testUser, taskWithUser);
        taskService.add(taskWithoutUser);
    }

    @After
    @SneakyThrows
    public void after() {
        taskService.clear();
        projectService.clear();
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByUserAndProject() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.deleteByUserAndProject(NULL_USER, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.deleteByUserAndProject(testUser, NULL_PROJECT));

        service.deleteByUserAndProject(testUser, project);
        Assert.assertEquals(0, taskService.findAllByUserAndProject(testUser, project).size());
    }

    @Test
    @SneakyThrows
    public void deleteAllByUser() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.deleteAllByUser(NULL_USER));

        service.deleteAllByUser(testUser);
        Assert.assertEquals(0, taskService.findAllByUserAndProject(testUser, project).size());
    }

}
