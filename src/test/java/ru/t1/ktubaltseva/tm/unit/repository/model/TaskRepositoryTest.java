package ru.t1.ktubaltseva.tm.unit.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.model.TaskRepository;
import ru.t1.ktubaltseva.tm.repository.model.UserRepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

import static ru.t1.ktubaltseva.tm.constant.model.TaskTestData.*;
import static ru.t1.ktubaltseva.tm.constant.model.UserTestData.TEST_USER_LOGIN;
import static ru.t1.ktubaltseva.tm.constant.model.UserTestData.TEST_USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class TaskRepositoryTest {

    @NotNull
    private final Task taskWithUser = MODEL_1;

    @NotNull
    private final Task taskWithoutUser = MODEL_2;

    @NotNull
    private final Project project = PROJECT_1;

    @Nullable
    private User testUser;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        projectRepository.saveAndFlush(project);

        testUser = userRepository.findById(UserUtil.getUserId()).get();
        taskWithUser.setUser(testUser);
        taskWithUser.setProject(project);

        repository.saveAndFlush(taskWithUser);
        repository.saveAndFlush(taskWithoutUser);
    }

    @After
    @SneakyThrows
    public void after() {
        repository.deleteAll();
        projectRepository.delete(project);
    }

    @Test
    @SneakyThrows
    public void countByUser() {
        Assert.assertEquals(1, repository.countByUser(testUser));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteAllByUser() {
        Assert.assertEquals(1, repository.countByUser(testUser));
        repository.deleteAllByUser(testUser);
        Assert.assertEquals(0, repository.countByUser(testUser));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByUserAndId() {
        Assert.assertEquals(1, repository.countByUser(testUser));
        repository.deleteByUserAndId(testUser, taskWithUser.getId());
        Assert.assertEquals(0, repository.countByUser(testUser));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    public void existsByUserAndId() {
        Assert.assertTrue(repository.existsByUserAndId(testUser, taskWithUser.getId()));
        Assert.assertFalse(repository.existsByUserAndId(testUser, NON_EXISTENT_MODEL_ID));
        Assert.assertFalse(repository.existsByUserAndId(testUser, taskWithoutUser.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllByUser() {
        @NotNull final List<Task> tasks = repository.findAllByUser(testUser);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findByUserAndId() {
        Assert.assertTrue(repository.findByUserAndId(testUser, taskWithUser.getId()).isPresent());
        Assert.assertFalse(repository.findByUserAndId(testUser, taskWithoutUser.getId()).isPresent());
    }

    @Test
    @SneakyThrows
    public void countByUserAndProject() {
        Assert.assertEquals(1, repository.countByUserAndProject(testUser, project));
    }

    @Test
    @SneakyThrows
    public void findAllByUserAndProject() {
        @NotNull final List<Task> tasks = repository.findAllByUserAndProject(testUser, project);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteAllByUserAndProject() {
        Assert.assertEquals(1, repository.countByUserAndProject(testUser, project));
        repository.deleteAllByUserAndProject(testUser, project);
        Assert.assertEquals(0, repository.countByUserAndProject(testUser, project));
        Assert.assertEquals(1, repository.count());
    }

}
