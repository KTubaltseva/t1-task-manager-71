package ru.t1.ktubaltseva.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class UserRestEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080/api/users";

    @NotNull
    private final UserDTO user = new UserDTO();

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserDTOService service;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    @SneakyThrows
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        user.setLogin(USER_1_LOGIN);
        user.setPasswordHash(passwordEncoder.encode(USER_1_PASSWORD));
        user.setEmail(USER_1_EMAIL);

        service.add(user);
    }

    @After
    @SneakyThrows
    public void after() {
        try {
            service.delete(user);
        } catch (Exception e) {
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final String url = URL + "/add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(USER_LOGIN);
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.put(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final UserDTO userRs = objectMapper.readValue(jsonRs, UserDTO.class);
        Assert.assertNotNull(service.findById(user.getId()));
        Assert.assertEquals(userRs.getId(), user.getId());

        service.delete(userRs);
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = URL + "/delete/" + user.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(user.getId()));
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final String url = URL + "/exists/" + user.getId();
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        final boolean rs = objectMapper.readValue(jsonRs, Boolean.class);
        Assert.assertTrue(rs);
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final String url = URL + "/find/" + user.getId();
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final UserDTO userRs = objectMapper.readValue(jsonRs, UserDTO.class);
        Assert.assertNotNull(service.findById(userRs.getId()));
        Assert.assertEquals(userRs.getId(), user.getId());
    }

    @Test
    @SneakyThrows
    public void update() {
        @NotNull final String url = URL + "/update";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final UserDTO user = this.user;
        user.setLogin(USER_LOGIN);
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final UserDTO userRs = objectMapper.readValue(jsonRs, UserDTO.class);
        Assert.assertNotNull(service.findById(user.getId()));
        Assert.assertEquals(userRs.getId(), user.getId());
        Assert.assertEquals(userRs.getLogin(), USER_LOGIN);
        Assert.assertEquals(service.findById(user.getId()).getLogin(), USER_LOGIN);
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = URL + "/findAll";
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final List<UserDTO> usersRs = Arrays.asList(objectMapper.readValue(jsonRs, UserDTO[].class));
        Assert.assertNotNull(usersRs);
        Assert.assertEquals(3, usersRs.size());
    }

    @Test
    @Ignore
    @SneakyThrows
    public void clear() {
        @NotNull final String url = URL + "/clear";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, service.count());
    }

    @Test
    @SneakyThrows
    public void count() {
        @NotNull final String url = URL + "/count";
        @NotNull final String jsonRs = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        final long rs = objectMapper.readValue(jsonRs, Long.class);
        Assert.assertEquals(3, rs);
    }

}
