package ru.t1.ktubaltseva.tm;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.t1.ktubaltseva.tm.unit.service.dto.ProjectDTOServiceTest;
import ru.t1.ktubaltseva.tm.unit.service.dto.TaskDTOServiceTest;
import ru.t1.ktubaltseva.tm.unit.service.dto.UserDTOServiceTest;
import ru.t1.ktubaltseva.tm.unit.service.model.ProjectServiceTest;
import ru.t1.ktubaltseva.tm.unit.service.model.TaskServiceTest;
import ru.t1.ktubaltseva.tm.unit.service.model.UserServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ProjectDTOServiceTest.class, TaskDTOServiceTest.class, UserDTOServiceTest.class, ProjectServiceTest.class, TaskServiceTest.class, UserServiceTest.class})
public class SuiteService {

}